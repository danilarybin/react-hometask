import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import Onelist from "./components/onelist";
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
      <Onelist />
  </React.StrictMode>
);


